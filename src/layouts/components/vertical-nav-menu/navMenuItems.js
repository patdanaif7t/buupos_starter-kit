/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

export default [
  {
    header: "สำหรับผู้จัดการ",
    items: [
      {
        url: "/MenuForManager",
        name: "หน้าหลัก",
        slug: "หน้าหลัก",
        icon: "GridIcon",
        i18n: "MenuForManager",
      },
      {
        url: "/ManageEmployee",
        name: "จัดการพนักงาน",
        slug: "จัดการพนักงาน",
        icon: "UserIcon",
        i18n: "ManageEmployee",
      },
      {
        url: "/ManageMenu",
        name: "จัดการรายการอาหาร",
        slug: "จัดการรายการอาหาร",
        icon: "ArchiveIcon",
        i18n: "จัดการรายการอาหาร",
      },
      {
        url: "/ManageTable",
        name: "จัดการโต๊ะ",
        slug: "จัดการโต๊ะ",
        icon: "GridIcon",
        i18n: "จัดการโต๊ะ",
      },
      {
        url: "/Report",
        name: "ดูประวัติการขาย",
        slug: "ดูประวัติการขาย",
        icon: "CalendarIcon",
        i18n: "ดูประวัติการขาย",
      }
    ]

  },
  {
    header: 'สำหรับผู้จัดการและพนักงาน',
    items: [
      {
        url: '/OrderTable',
        name: "บริการลูกค้า",
        slug: "บริการลูกค้า",
        icon: "SmileIcon",
        i18n: "บริการลูกค้า",
      }
    ]
  },
  {
    header: 'สำหรับผู้จัดการและครัว',
    items: [
      {
        url: '/Kitchen',
        name: "รายการอาหารที่ได้รับ",
        slug: "รายการอาหารที่ได้รับ",
        icon: "PlusCircleIcon",
        i18n: "รายการอาหารที่ได้รับ",
      },
      {
        url: '/KitchenHistory',
        name: "รายการอาหาร",
        slug: "รายการอาหาร",
        icon: "MenuIcon",
        i18n: "รายการอาหาร",
      }
    ]
  },
  {
    header: 'สำหรับผู้จัดการและแคชเชียร์',
    items: [
      {
        url: '/Cashier',
        name: "ชำระเงิน",
        slug: "ชำระเงิน",
        icon: "InfoIcon",
        i18n: "ชำระเงิน",
      }
    ]
  }
  
  
];
